<?php

use yii\db\Migration;

/**
 * Class m190627_082028_add_link_trip_service_flight_segment
 */
class m190627_082028_add_link_trip_service_flight_segment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->addColumn('{{%flight_segment}}', 'trip_service_id', $this->integer()->notNull()->defaultValue(0));
        $this->createIndex('idx_flight_segment_trip_service', '{{%flight_segment}}', 'trip_service_id');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('idx_flight_segment_trip_service', '{{%flight_segment}}');
        $this->dropColumn('{{%flight_segment}}', 'trip_service_id');
    }
}
