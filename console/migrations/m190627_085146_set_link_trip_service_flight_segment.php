<?php

use yii\db\Migration;

/**
 * Class m190627_085146_set_link_trip_service_flight_segment
 */
class m190627_085146_set_link_trip_service_flight_segment extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->execute(' 
            CREATE procedure setlink()
            BEGIN
                DECLARE lim smallint;
                if FLOOR(rand() * 100) > 50 THEN
                    SET lim = 2;
                ELSE 
                    SET lim = 3;
                end if;
                
                WHILE exists (SELECT fs.trip_service_id FROM flight_segment fs WHERE fs.trip_service_id = 0) DO
                    BEGIN
                        UPDATE flight_segment SET trip_service_id = (SELECT ts.id FROM trip_service ts ORDER BY RAND() LIMIT 1)
                        WHERE flight_segment.trip_service_id = 0 
                        ORDER BY RAND() DESC limit 3;
                    END;
                END WHILE;
            
            END;
            DELIMITER ;
            
            CALL setlink();
            
            DROP procedure setlink;
        ');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {

    }
}
