<?php

use yii\db\Migration;

/**
 * Class m190628_122124_alter_flight_segment_add_dep_airport_key
 */
class m190628_122124_alter_flight_segment_add_dep_airport_key extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createIndex('k_flight_segment_dep_airport_id', '{{%flight_segment}}', 'depAirportId');
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropIndex('k_flight_segment_dep_airport_id', '{{%flight_segment}}');
    }
}
