<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 28.06.19
 * Time: 12:53
 */
namespace console\controllers;

use common\models\AirportName;
use yii\base\ErrorException;
use yii\console\Controller;
use yii\helpers\VarDumper;

/**
 * Class AirportDataFillerController
 *
 * Usage:
 * ```
 *  php yii airport-data-filler/fill
 * ```
 *
 * @package console\controllers
 */
class AirportDataFillerController extends Controller
{
    public function actionFill()
    {
        $airports = AirportName::find()->all();
        $converter = \Yii::$container->get('dataConverter');

        $counter = 0;
        foreach ($airports as $airport) {
            $airport->mh_normalized_airport_name = join(' ', $converter->convert($airport->value));
            if ($airport->save()) {
                $counter++;
            } else {
                throw new ErrorException('something is wrong: ' . VarDumper::dumpAsString($airport->errors));
            }
        }

        echo 'All operations completed, rows updated: ' . $counter;
    }
}