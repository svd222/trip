<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 28.06.19
 * Time: 13:53
 */
namespace frontend\controllers;

use common\components\TripSearchService;
use common\models\Trip;
use yii\data\ActiveDataProvider;
use yii\web\Controller;
use Yii;

class TripController extends Controller
{
    public function actionIndex()
    {
        $airportName = Yii::$app->request->get('name', '');
        /**
         * @var TripSearchService $service
         */
        $service = Yii::createObject([
            'class' => TripSearchService::class,
            'airportName' => $airportName
        ]);

        $query = $service->search();

        $dataProvider = new ActiveDataProvider([
            'query' => $query,
            'pagination' => [
                'pageSize' => 20,
            ],
        ]);

        return $this->render('index', [
            'dataProvider' => $dataProvider
        ]);
    }
}