<?php

use yii\helpers\Html;
use yii\grid\GridView;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $dataProvider yii\data\ActiveDataProvider */

$this->title = 'Trips';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="trip-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <?php
        $form = ActiveForm::begin([
            'id' => 'search-form',
            'enableAjaxValidation' => false,
            'enableClientValidation' => false,
            'fieldConfig' => [
                'template' => '{input}',
                'options' => [
                    'tag' => false,
                ],
            ],
            'method' => 'get',
        ]);
    ?>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo Html::textInput('name', '', [
                    'id' => 'name'
                ]);
            ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <?php
                echo Html::submitButton();
            ?>
        </div>
    </div>
    <br />
    <?php
        ActiveForm::end();
    ?>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
            ['class' => 'yii\grid\SerialColumn'],

            'id',
            'corporate_id',
            'number',
            'user_id',
            [
                'attribute' => 'created_at',
                'value' => function($model, $key, $index) {
                    $value = $model->created_at;
                    if ($value) {
                        return date('d/m/Y H:i:s');
                    } else {
                        return '';
                    }
                }
            ],
            [
                'attribute' => 'updated_at',
                'value' => function($model, $key, $index) {
                    $value = $model->updated_at;
                    if ($value) {
                        return date('d/m/Y H:i:s');
                    } else {
                        return '';
                    }
                }
            ],
            //'coordination_at',
            [
                'attribute' => 'saved_at',
                'value' => function($model, $key, $index) {
                    $value = $model->saved_at;
                    if ($value) {
                        return date('d/m/Y H:i:s');
                    } else {
                        return '';
                    }
                }
            ],
            //'tag_le_id',
            //'trip_purpose_id',
            //'trip_purpose_parent_id',
            //'trip_purpose_desc:ntext',
            'status',

            ['class' => 'yii\grid\ActionColumn'],
        ],
    ]); ?>


</div>