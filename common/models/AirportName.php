<?php

namespace common\models;

use Yii;

/**
 * This is the model class for table "{{%airport_name}}".
 *
 * @property int $id
 * @property int $airport_id
 * @property int $language_id
 * @property string $value
 * @property string $mh_normalized_airport_name
 */
class AirportName extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%airport_name}}';
    }

    /**
     * @return \yii\db\Connection the database connection used by this AR class.
     */
    public static function getDb()
    {
        return Yii::$app->get('airport_db');
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['airport_id', 'value'], 'required'],
            [['airport_id', 'language_id'], 'integer'],
            [['value'], 'string', 'max' => 255],
            [['mh_normalized_airport_name'], 'string', 'max' => 64],
            [['airport_id', 'language_id'], 'unique', 'targetAttribute' => ['airport_id', 'language_id']],
        ];
    }

    public function beforeSave($insert)
    {
        if ($insert || (!$insert && !empty($this->getDirtyAttributes(['value'])))) {
            /**
             * @var \common\interfaces\DataConverterInterface $converter
             */
            $converter = \Yii::$container->get('dataConverter');
            $this->mh_normalized_airport_name = join(' ', $converter->convert($this->value));
        }
        return parent::beforeSave($insert);
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'airport_id' => 'Airport ID',
            'language_id' => 'Language ID',
            'value' => 'Value',
            'mh_normalized_airport_name' => 'Mh Normalized Airport Name',
        ];
    }

    /**
     * {@inheritdoc}
     * @return AirportNameQuery the active query used by this AR class.
     */
    public static function find()
    {
        return new AirportNameQuery(get_called_class());
    }
}
