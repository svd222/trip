<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 27.06.19
 * Time: 16:09
 */
namespace common\components;

use common\interfaces\DataConverterInterface;
use yii\base\BaseObject;
use yii\validators\NumberValidator;
use Yii;

/**
 * Class DataPreparator
 *
 * Usage exapmle
 *
 * ```
 *  $converter = \Yii::$container->get('dataConverter');
 *  $str = 'некоторая подборка приколов';
 *  $converted = join(' ', $converter->convert($str));
 * ```
 *
 * @package common\components
 */
class DataConverter extends BaseObject implements DataConverterInterface
{
    public $minLen = 2;

    public $minNormalizedLen = 2;

    public $maxNormalizedLen = 4;

    /**
     * Returns converted value based on a metaphone function @see @link https://php.net/manual/ru/function.metaphone.php
     *
     * @param $str
     * @param bool $validate
     * @return string
     */
    public function metaphone(string $str, bool $validate = true): string
    {
        $mh = metaphone($str);

        if ($validate) {
            $validator = new NumberValidator([
                'min' => $this->minLen,
                'integerOnly' => true,
            ]);

            if (!$validator->validate(strlen($mh))) {
                return '';
            }
        }
        return $mh;
    }

    /**
     * Normalize metaphone:
     *  remove 1 vowel from start of string (if present)
     *  cut the metaphone to [[\common\components\Similarity::$maxNormalizedMetaphoneLen]]
     *
     * @param string $metaphone
     * @return bool|string
     */
    public function normalize(string $metaphone): string
    {
        $len = strlen($metaphone);
        if ($len > $this->minNormalizedLen) {
            $search = ['A', 'E', 'O', 'I', 'U', 'Y'];
            $firstSym = substr($metaphone, 0, 1);
            if (in_array($firstSym, $search)) {
                $metaphone = substr($metaphone, 1);
                $len--;
            }
        }

        if ($len > $this->maxNormalizedLen) {
            $metaphone = substr($metaphone, 0, $this->maxNormalizedLen);
        }

        return $metaphone;
    }

    /**
     * Prepare inbound string into normalized set
     *
     * @param string $str
     * @return array
     */
    public function convert(string $str): array
    {
        $translit = Yii::$container->get('transliterator');
        $translitted = $translit->translit($str);
        $translitted = preg_replace('/[,\.]/mis', ' ',$translitted);
        $translitted = preg_split('/\s+/mis', $translitted);

        $mh = [];
        for ($i = 0; $i < count($translitted); $i++) {
            $mh[] = $this->metaphone($translitted[$i]) ?? '';
        }

        $mhNormalized = [];
        for ($i = 0; $i < count($mh); $i++) {
            $v = $this->normalize($mh[$i]) ?? '';
            if ($v) {
                $mhNormalized[] = $v;
            }
        }
        return $mhNormalized;
    }
}