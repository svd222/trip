<?php
/**
 * Created by PhpStorm.
 * @author: svd22286@gmail.com
 * @date: 27.06.19
 * @time: 15:59
 */
namespace common\components;

use common\interfaces\TransliteratorInterface;
use Yii;
use yii\base\BaseObject;
use yii\base\InvalidConfigException;

/**
 * Class Translit, translit string from one lang to another
 *
 * Usage:
 * Assume that we have 'translit' section in params:
 * common/config/params-local.php
 * ```
 * return [
 *      ...
 *      'translit' => [
 *          'ru_en' => [
                [
 *                  'А', 'Б', 'В', ...
 *              ],
 *              [
 *                  'A', 'B', 'V', ...
 *              ],
 *          ]
 *      ],
 *      ...
 * ];
 * ```
 *
 * ```
 * $translit = \Yii::createObject(Translit::class, ['ru_en']);
 * $translitted = $translit->translit('Некоторый текст для транслитерации с русского на английский');
 * ```
 */
class Translit extends BaseObject implements TransliteratorInterface
{
    /**
     * @var [] $translitTable
     */
    public $translitTable;

    /**
     * Translit constructor.
     *
     * @param string $direction
     * @param array $config
     * @throws InvalidConfigException
     */
    public function __construct(string $direction, $config = []) {
        $direction = strtolower($direction);
        if (!isset(Yii::$app->params['translit']) || !isset(Yii::$app->params['translit'][$direction])) {
            throw new InvalidConfigException('Translit table for `'.$direction.'` is missing`');
        }
        $this->translitTable = Yii::$app->params['translit'][$direction];
        parent::__construct($config);
    }

    /**
     * Translit from one lang to another.
     *
     * @param $str
     * @return mixed
     */
    public function translit(string $str): string
    {
        return str_replace($this->translitTable[0], $this->translitTable[1], $str);
    }
}