<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 28.06.19
 * Time: 14:31
 */
namespace common\components;

use common\models\Trip;
use Yii;
use yii\base\Component;
use yii\db\ActiveQuery;

class TripSearchService extends Component
{
    public $serviceId = 2;

    public $corporateId = 3;

    public $airportName;

    public function __construct(array $config = [])
    {
        parent::__construct($config);
    }

    /**
     * @return \common\models\TripQuery
     */
    public function search()
    {
        /**
         * @var \common\components\Similarity $similarity
         */
        $similarity = Yii::createObject([
            'class' => \common\components\Similarity::class,
            'idx' => 'airport'
        ]);
        $searchResults = $similarity->search($this->airportName, 'mh_normalized_airport_name');
        $similarity = 0;
        $airport = null;
        foreach ($searchResults as $sR) {
            if ($similarity < $sR['similarity']) {
                $similarity = $sR['similarity'];
                $airport = $sR['model'];
            }
        }
        if ($airport) {
            $airportId = intval($airport['airport_id']);

            $serviceId = $this->serviceId;
            $query = Trip::find()
                ->innerJoinWith([
                    'tripServices' => function($query) use($serviceId, $airportId) {
                        /**
                         * @var ActiveQuery $query
                         */
                        $query->alias('ts');
                        $query->where('ts.service_id = :serviceId', [
                            'serviceId' => $serviceId
                        ]);
                        $query->innerJoinWith([
                            'flightSegments' => function($query) use ($airportId) {
                                /**
                                 * @var ActiveQuery $query
                                 */
                                $query->alias('fs');
                                $query->where('fs.depAirportId = :airportId', [
                                    'airportId' => $airportId
                                ]);
                            }
                        ]);
                    }
                ])
                ->alias('t')
                ->where('t.corporate_id = :corporateId', [
                    'corporateId' => $this->corporateId
                ]);
            return $query;
        } else {
            return Trip::find();
        }
    }
}