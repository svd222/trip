<?php
Yii::setAlias('@common', dirname(__DIR__));
Yii::setAlias('@frontend', dirname(dirname(__DIR__)) . '/frontend');
Yii::setAlias('@backend', dirname(dirname(__DIR__)) . '/backend');
Yii::setAlias('@console', dirname(dirname(__DIR__)) . '/console');

Yii::$container->setDefinitions([
    \common\interfaces\TransliteratorInterface::class => function($container, $params, $config) {
        return new \common\components\Translit(Yii::$app->params['translitDirection']);
    }
]);

Yii::$container->set('transliterator', \common\interfaces\TransliteratorInterface::class);

Yii::$container->set(\common\interfaces\DataConverterInterface::class, \common\components\DataConverter::class);
Yii::$container->set('dataConverter', \common\interfaces\DataConverterInterface::class);