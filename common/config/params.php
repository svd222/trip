<?php
return [
    'adminEmail' => 'admin@example.com',
    'supportEmail' => 'support@example.com',
    'senderEmail' => 'noreply@example.com',
    'senderName' => 'Example.com mailer',
    'user.passwordResetTokenExpire' => 3600,
    'translit' => [
        'ru_en' => [
            [
                'А', 'Б', 'В', 'Г', 'Д', 'Е', 'Ё', 'Ж', 'З', 'И',
                'Й', 'К', 'Л', 'М', 'Н', 'О', 'П', 'Р', 'С', 'Т',
                'У', 'Ф', 'Х', 'Ц', 'Ч', 'Ш', 'Щ', 'Ъ', 'Ы', 'Ь',
                'Э', 'Ю', 'Я', 'а', 'б', 'в', 'г', 'д', 'е', 'ё',
                'ж', 'з', 'и', 'й', 'к', 'л', 'м', 'н', 'о', 'п',
                'р', 'с', 'т', 'у', 'ф', 'х', 'ц', 'ч', 'ш', 'щ',
                'ъ', 'ы', 'ь', 'э', 'ю', 'я'
            ],
            [
                'A', 'B', 'V', 'G', 'D', 'E', 'E', 'Gh', 'Z', 'I',
                'Y', 'K', 'L', 'M', 'N', 'O', 'P', 'R', 'S', 'T',
                'U', 'F', 'H', 'C', 'Ch', 'Sh', 'Sch', '', 'I',
                '', 'E', 'Yu', 'Ya', 'a', 'b', 'v', 'g', 'd', 'e',
                'e', 'gh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o',
                'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh',
                'sch', '', 'i', '', 'e', 'yu', 'ya'
            ],
        ]
    ],
    'translitDirection' => 'ru_en',
];
