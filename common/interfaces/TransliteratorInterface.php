<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 27.06.19
 * Time: 17:27
 */
namespace common\interfaces;

interface TransliteratorInterface
{
    public function translit(string $str): string;
}