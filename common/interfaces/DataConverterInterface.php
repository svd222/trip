<?php
/**
 * Created by PhpStorm.
 * User: svd
 * Date: 27.06.19
 * Time: 16:36
 */
namespace common\interfaces;

interface DataConverterInterface
{
    /**
     * Returns converted value
     *
     * @param string $str
     * @return array
     */
    public function convert(string $str): array ;
}