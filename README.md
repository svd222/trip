

# HOW TO USE #

After clone project:
1. create databases: `trip`, `airport` with appropriate data from dump
2. fix the config of db/sphinx connections `common/config/main-local.php` (see config example below)
3. `$ composer install`
4. `$ php yii migrate`
5. create folder in root folder called `sphinx` for sphinx data (with appropriate permissions)
6. execute console command: `php yii airport-data-filler/fill` to fill data needed for further search
7. `sudo /etc/init.d/sphinxsearch stop`
8. `sudo indexer --all`
9. `sudo /etc/init.d/sphinxsearch start`

## PROFIT!!! ##


#### Config example of `common/config/main-local.php` ####

```
'components' => [
        'db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=trip',
            'username' => 'root',
            'password' => 'YOUR PASSWORD HERE',
            'charset' => 'utf8',
        ],
        'airport_db' => [
            'class' => 'yii\db\Connection',
            'dsn' => 'mysql:host=localhost;dbname=airport',
            'username' => 'root',
            'password' => 'YOUR PASSWORD HERE',
            'charset' => 'utf8',
        ],
        'sphinx' => [
            'class' => 'yii\sphinx\Connection',
            'dsn' => 'mysql:host=0;port=9306;',
            'username' => 'root',
            'password' => 'YOUR PASSWORD HERE',
        ],
        ....
    ],
```
 